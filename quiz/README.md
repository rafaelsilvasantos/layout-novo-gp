# grandepremio_quiz

Instalação e configuração:
1 - Editar o arquivo de configuração (_theme/config/.config.json)
"bs": {
    "proxy": "CAMINHO URL",
    "port": "PORTA",
    "browser": "chrome"
}

2 - Na pasta "_theme/" instalar o npm.
npm install
npm start

Pronto!

Obs:
1 - Editar os arquivos dentro de _theme e não na pasta "src".
2 - Arquivos de fonte (ttf,svg,woff entre outros) e novas extensões (.php, .txt, .ico) devem ser configurado no gulpfile.js ou copiado manualmente para pasta "src/"
