$(document).ready(function(){
	$(".bloco-mais-noticias").click(function(){
		  $(this).addClass("hidden");
		  $(".bloco-mais-noticias-wrapper").addClass("hidden");
		  $(".secundarias").removeClass("hidden");
		  $(".secundarias .bloco-mais-noticias-wrapper").removeClass("hidden");
	 });
	 $(".tabela-pilotos-completa").click(function(){
		  $(this).addClass("hidden");
		  $(".tabela-pilotos").removeClass("hidden");
	 });
	 $(".tabela-equipes-completa").click(function(){
		  $(this).addClass("hidden");
		  $(".tabela-equipes").removeClass("hidden");
	 });
	 $(".icone-busca").click(function(){
		 if($(this).hasClass("open")){
			$(this).removeClass("open");
			$(".menu-busca").addClass("hidden");
		 }
		  else {
			$(this).addClass("open");
			$(".menu-busca").removeClass("hidden");
				if($(".explorar").hasClass("open")){
					$(".explorar").removeClass("open");
					$(".menu-explorar").addClass("hidden");
				}
			};
		});
	  $(".explorar").click(function(){
		 if($(this).hasClass("open")){
			$(this).removeClass("open");
			$(".menu-explorar").addClass("hidden");
		 }
		  else {
			$(this).addClass("open");
			$(".menu-explorar").removeClass("hidden");
				if($(".icone-busca").hasClass("open")){
					$(".icone-busca").removeClass("open");
					$(".menu-busca").addClass("hidden");
				}
		 };
	  });
	  $(".corrida-1").click(function(){
		  $(this).removeClass("hidden");
		  $(".corrida-1").addClass("active");
		  $(".corrida-1").addClass("f1");
		  $(".corrida-2").removeClass("active");
		  $(".corrida-2").removeClass("f1");
		  $(".ultimas-etapas-1").removeClass("hidden");
		  $(".ultimas-etapas-2").addClass("hidden");
	  });
	  $(".corrida-2").click(function(){
		  $(this).removeClass("hidden");
		  $(".corrida-2").addClass("active");
		  $(".corrida-2").addClass("f1");
		  $(".corrida-1").removeClass("active");
		  $(".corrida-1").removeClass("f1");
		  $(".ultimas-etapas-2").removeClass("hidden");
		  $(".ultimas-etapas-1").addClass("hidden");
	  });
	  $(".seletor-1").click(function(){
		  $(this).removeClass("hidden");
		  $(".seletor-1").addClass("active");
		  $(".seletor-2").removeClass("active");
		  $(".seletor-3").removeClass("active");
		  $(".seletor-4").removeClass("active");
		  $(".seletor-5").removeClass("active");
		  $(".ultimas-etapas-1").removeClass("hidden");
		  $(".ultimas-etapas-2").addClass("hidden");
		  $(".ultimas-etapas-3").addClass("hidden");
		  $(".ultimas-etapas-4").addClass("hidden");
		  $(".ultimas-etapas-5").addClass("hidden");
	  });
	  $(".seletor-2").click(function(){
		  $(this).removeClass("hidden");
		  $(".seletor-2").addClass("active");
		  $(".seletor-1").removeClass("active");
		  $(".seletor-3").removeClass("active");
		  $(".seletor-4").removeClass("active");
		  $(".seletor-5").removeClass("active");
		  $(".ultimas-etapas-2").removeClass("hidden");
		  $(".ultimas-etapas-1").addClass("hidden");
		  $(".ultimas-etapas-3").addClass("hidden");
		  $(".ultimas-etapas-4").addClass("hidden");
		  $(".ultimas-etapas-5").addClass("hidden");
	  });
	  $(".seletor-3").click(function(){
		  $(this).removeClass("hidden");
		  $(".seletor-3").addClass("active");
		  $(".seletor-1").removeClass("active");
		  $(".seletor-2").removeClass("active");
		  $(".seletor-4").removeClass("active");
		  $(".seletor-5").removeClass("active");
		  $(".ultimas-etapas-3").removeClass("hidden");
		  $(".ultimas-etapas-1").addClass("hidden");
		  $(".ultimas-etapas-2").addClass("hidden");
		  $(".ultimas-etapas-4").addClass("hidden");
		  $(".ultimas-etapas-5").addClass("hidden");
	  });
	  $(".seletor-4").click(function(){
		  $(this).removeClass("hidden");
		  $(".seletor-4").addClass("active");
		  $(".seletor-1").removeClass("active");
		  $(".seletor-2").removeClass("active");
		  $(".seletor-3").removeClass("active");
		  $(".seletor-5").removeClass("active");
		  $(".ultimas-etapas-4").removeClass("hidden");
		  $(".ultimas-etapas-1").addClass("hidden");
		  $(".ultimas-etapas-2").addClass("hidden");
		  $(".ultimas-etapas-3").addClass("hidden");
		  $(".ultimas-etapas-5").addClass("hidden");
	  });
	  $(".seletor-5").click(function(){
		  $(this).removeClass("hidden");
		  $(".seletor-5").addClass("active");
		  $(".seletor-1").removeClass("active");
		  $(".seletor-2").removeClass("active");
		  $(".seletor-3").removeClass("active");
		  $(".seletor-4").removeClass("active");
		  $(".ultimas-etapas-5").removeClass("hidden");
		  $(".ultimas-etapas-1").addClass("hidden");
		  $(".ultimas-etapas-2").addClass("hidden");
		  $(".ultimas-etapas-3").addClass("hidden");
		  $(".ultimas-etapas-4").addClass("hidden");
	  });
      $('.slider').bxSlider(
		{
		  controls: false,
		  auto: true,
		  responsive: true,
		  preloadImages: 'all',
		  speed: 600,
		  autoHover: true,
		  infiniteLoop: false
		}
	);
	
	var height1 = $(window).height();
	var width1 = $(window).width();
	var heightMenuMob = height1 - 50
	
	$('.bloco-topstory.desktop .bx-viewport').css('height',height1+'px');
	$('.bloco-topstory.desktop .topstory').css('height',height1+'px');
	$('.content-menu-mobile').css('height',heightMenuMob+'px');
	
	$(".icone-menu").click(function(){
		 if($(".content-menu-mobile").hasClass("hidden")){
			$(".content-menu-mobile").removeClass("hidden");
			$(this).addClass("active");
		 }
		  else {
			$(".content-menu-mobile").addClass("hidden");
			$(this).removeClass("active");
		  };
		});
		
	// Define Galleria height
	var galleriaWidth = $('.galleria').width();
	var galleriaHeight = galleriaWidth / 1.5;
	$('.galleria').css('height', galleriaHeight+'px');
	
	// Load the Twelve theme
	Galleria.loadTheme('/js/galleria/themes/twelve/galleria.twelve.min.js');

	// Configuration	
	Galleria.configure({
		transition: 'fade',
		variation: 'light'
	});
	
    if ($('.galleria').length) {
        // Initialize Galleria
        Galleria.run('.galleria');
    }

 });
 
 $(window).resize(function(){
	 
	var height1 = $(window).height();
	var width1 = $(window).width();
	var heightMenuMob = height1 - 50
	
	$('.bloco-topstory.desktop .bx-viewport').css('height',height1+'px');
	$('.bloco-topstory.desktop .topstory').css('height',height1+'px');
	$('.content-menu-mobile').css('height',heightMenuMob+'px');
	
	var galleriaWidth = $('.galleria').width();
	var galleriaHeight = galleriaWidth / 1.5;
	$('.galleria').css('height', galleriaHeight+'px');

 });
 
$(function(){
 var shrinkHeader = 80;
  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
           $('.menu-principal').addClass('shrink');
		   $('.noticia .menu-principal .container').addClass('hidden');
		   $('.equipes .menu-principal .container').addClass('hidden');
        }
        else {
            $('.menu-principal').removeClass('shrink');
			$('.noticia .menu-principal .container').removeClass('hidden');
			$('.equipes .menu-principal .container').removeClass('hidden');
        }
  });
function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
    }
});
$(function() {
    
});
